from bs4 import BeautifulSoup
import requests
import csv
import time

the_file = './principals.csv'
the_file2 = './principals2.csv'
unique_id = './unique_id.csv'

with open(the_file2, 'w', newline='') as obj2:
    writer = csv.writer(obj2, delimiter=",")

    with open(the_file) as obj:
        reader = csv.reader(obj, delimiter=",", skipinitialspace=True)
        data = list(reader)
        i = 1
        for principlas in data[1:]:
            category = principlas[3]

            if category == 'actor' or category == 'director':
                writer.writerows([principlas])
            elif category == 'actress':
                principlas[3] = 'actor'
                writer.writerows([principlas])

with open(unique_id, 'w', newline='') as obj2:
    writer = csv.writer(obj2, delimiter=",")

    person_id = []

    with open(the_file2) as obj:
        reader = csv.reader(obj, delimiter=",", skipinitialspace=True)
        data = list(reader)
        for principlas in data[:]:
            person_id.append(principlas[2])

    person_set = set(person_id)

    writer.writerows([person_set])
