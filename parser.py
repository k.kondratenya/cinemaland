from bs4 import BeautifulSoup
import requests
import csv
import time

the_file = './names.csv'
the_file2 = './names2.csv'
unique_id = './unique_id.csv'

url = 'https://www.imdb.com/'
i = 1
with open(the_file2, 'w', newline='') as obj2:
    writer = csv.writer(obj2, delimiter=",")
    unique_ids = []

    with open(unique_id) as obj:
        reader = csv.reader(obj, delimiter=",", skipinitialspace=True)
        data = list(reader)
        unique_ids = data[0]

    with open(the_file) as obj:
        reader = csv.reader(obj, delimiter=",", skipinitialspace=True)
        data = list(reader)

        for person in data[1:]:
            try:
                if i % 50 == 0:
                    time.sleep(5)
                person_id = person[0]
                if person_id not in unique_ids:
                    continue

                try:
                    page = requests.get(f'{url}name/{person_id}')
                except:
                    continue
                print(person_id)
                print(page)

                soup = BeautifulSoup(page.text, 'html.parser')
                image = soup.find('div', {"class": "image"})
                if (image is None):
                    continue
                a = image.find('a')
                if (a is None):
                    continue

                src = a['href']
                try:
                    page = requests.get(f'{url}{src}')
                except:
                    continue

                soup = BeautifulSoup(page.text, 'html.parser')


                image = soup.find('img')
                if (image is None):
                    continue

                person.append(image['src'])
                writer.writerows([person])
                i += 1
            except:
                continue

# with open(the_file) as obj:
#     reader = csv.reader(obj, delimiter=",", skipinitialspace=True)
#     data = list(reader)
#     # print(len(data[0]))
#     for dat in data[1]:
#         print(dat)

with open(unique_id) as obj:
    reader = csv.reader(obj, delimiter=",", skipinitialspace=True)
    print(len(list(reader)[0]))
