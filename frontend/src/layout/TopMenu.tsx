import React, { ReactElement } from 'react';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import UserMenu from '../components/topMenu/UserMenu';
import Search from '../components/topMenu/Search';

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  drawer: {
  },
  appBar: {
    [theme.breakpoints.up('lg')]: {
      zIndex: theme.zIndex.drawer + 1,
    },
  },
  header: {
    display: 'flex',
    justifyContent: 'space-between',
    maxWidth: '1400px',
    width: '100%',
    margin: 'auto',
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  // necessary for content to be below app bar
  toolbar: theme.mixins.toolbar,
  drawerPaper: {
    width: drawerWidth,
  },
  content: {
    padding: theme.spacing(3),
    maxWidth: '1400px',
    width: '100%',
    margin: 'auto',
  },
}));

function ResponsiveDrawer(props: { children: ReactElement }) {
  const { children } = props;
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar className={classes.header}>
          <div className="flx-aic">
            <Button component={Link} color="inherit" to="/">
              Cinemaland
            </Button>
          </div>
          <div className="flx-aic">
            <Search />
            <UserMenu />
          </div>
        </Toolbar>
      </AppBar>
      <main className={classes.content}>
        <div className={classes.toolbar} />
        { children }
      </main>
    </div>
  );
}

export default ResponsiveDrawer;
