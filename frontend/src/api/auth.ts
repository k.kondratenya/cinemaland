import { User } from '../hooks/auth';
import { AxiosRequestConfig, rest } from '../utils/rest';

interface AuthReq {
  email: string;
  password: string;
}

interface SignUpRequest {
  email: string;
  username: string;
  password: string;
  confirmPassword: string;
}

interface UpdateUserRequest {
  email: string;
  username: string
}

export const getUser = async (config?: AxiosRequestConfig) => rest.get<User>('/user/', config);

export const updateUser = async (param: UpdateUserRequest, config?: AxiosRequestConfig) => rest.patch('/user/', config);

export const login = async (data: AuthReq, config?: AxiosRequestConfig) => rest.post<User>('/user/login/', data, config);

export const logout = async (config?: AxiosRequestConfig) => rest.post<''>('/user/logout/', {}, config);

export const signUp = async (data: SignUpRequest, config?: AxiosRequestConfig) => rest.post<''>('/user/sign-up/', data, config);
