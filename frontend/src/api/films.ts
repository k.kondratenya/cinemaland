import { rest, AxiosRequestConfig } from '../utils/rest';

export type Film = {
  title: string,
  poster: string,
  year: number,
  id: number,
  isFavorite: boolean,
  watchLater: boolean,
}

export type FilmRes = Film[]

export const filmFields = ['released', 'genre', 'boxOffice', 'country', 'production', 'runtime'] as const;

export type FilmActions = {
  isFavorite: boolean;
  watchLater: boolean;
  watched: boolean;
  userRating: null | number;
}

export type FilmRate = {
  watched: boolean;
  userRating: null | number;
}

export type PartialFilmRate = Partial<FilmRate>;

type PersonBase = {
  personID: number,
  role: string | null,
  name: string,
}

export type FilmActor = PersonBase & {
  job: 'actor',
}

export type FilmDirector = PersonBase & {
  job: 'director',
}

export type FilmPerson = FilmActor | FilmDirector;

export type FilmResponse = Film & {
  plot: string;
  imdbRating: number;
  persons: FilmPerson[];
} & Record<typeof filmFields[number], string> & FilmActions;

export type FilmExtended = Film & {
  plot: string;
  imdbRating: number;
  actors: FilmActor[];
  directors: FilmDirector[];
} & Record<typeof filmFields[number], string> & FilmActions;

export type PartialFilmActions = Partial<FilmActions>

export type Rate = -1 | 1;

export type RateExtended = Rate | 0;

export type Comment = {
  id: number;
  username: string;
  content: string;
  date: string;
  rate: RateExtended;
  sumRate: number;
}

export type CommentRes = Comment[]

export type PostComment = {
  content: string;
}

export type PostCommentRate = {
  rate: RateExtended;
}

export const getFilms = async (config?: AxiosRequestConfig) => rest.get<FilmRes>('/film/', config);

export const getFilm = async (id: number, config?: AxiosRequestConfig) => rest.get<FilmResponse>(`/film/${id}/`, config);

export const filmSearch = async (name: string, config?: AxiosRequestConfig) => rest.get<Film[]>(`/film/search?q=${name}`, config);

export const setFilmAction = async (id: number, data: PartialFilmActions, config?: AxiosRequestConfig) => rest.post<''>(`/film/${id}/action/`, data, config);

export const setFilmRate = async (id: number, data: PartialFilmRate, config?: AxiosRequestConfig) => rest.post<FilmRate>(`/film/${id}/rate/`, data, config);

export const getFilmComments = async (id: number, config?: AxiosRequestConfig) => rest.get<CommentRes>(`/comment/${id}/`, config);

export const postFilmComments = async (id: number, data: PostComment, config?: AxiosRequestConfig) => rest.post<Comment>(`/comment/${id}/`, data, config);

export const postCommentRate = async (id: number, data: PostCommentRate, config?: AxiosRequestConfig) => rest.post<''>(`/comment/${id}/rate/`, data, config);
