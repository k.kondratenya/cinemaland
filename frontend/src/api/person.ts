import { rest, AxiosRequestConfig } from '../utils/rest';
import { Film } from './films';

export type Person = {
  name: string;
  bio: string;
  poster: string;
} & Record<typeof personFields[number], string>

export const personFields = ['name', 'birthDetails', 'deathDetails'] as const;

export const getPerson = async (param: number, config?: AxiosRequestConfig) => rest.get<Person>(`/person/${param}/`, config);

export const getPersonFilms = async (param: number, config?: AxiosRequestConfig) => rest.get<Film[]>(`/person/${param}/films/`, config);
