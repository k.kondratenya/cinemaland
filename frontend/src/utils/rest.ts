import Axios, {
  AxiosResponse, AxiosRequestConfig as _AxiosRequestConfig,
} from 'axios';

// reexport types;
export type AxiosRequestConfig = _AxiosRequestConfig;

const devConfig: AxiosRequestConfig = {
  withCredentials: true,
};

const HTTP_URL = '/api';

function getCookie(name: string) {
  const matches = document.cookie.match(new RegExp(
    // eslint-disable-next-line no-useless-escape
    `(?:^|; )${name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1')}=([^;]*)`,
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}

interface AxiosErrorDataJson {
  error: {
    msg: string;
  };
}
type AxiosErrorData = AxiosErrorDataJson | '';
export type AxiosSucceedResponse<T> = AxiosResponse<T> & { state: 'success' };
export type AxiosFailedResponse = Partial<AxiosResponse<AxiosErrorData>> & { state: 'failed' };
export type AxiosReply<T extends object | ''> = Promise<AxiosSucceedResponse<T> | AxiosFailedResponse>;

const wrapper1 = (func: typeof Axios.get) => async <T extends object | ''>(path: string, config?: AxiosRequestConfig): Promise<AxiosSucceedResponse<T> | AxiosFailedResponse> => {
  try {
    const response = await func<T>(HTTP_URL + path, { ...config, ...devConfig });
    return {
      ...response,
      state: 'success',
    };
  } catch (error) {
    if (error.response === undefined) {
      return {
        state: 'failed',
      };
    }

    return {
      state: 'failed',
    };
  }
};
const wrapper2 = (func: typeof Axios.post) => async <T extends object | ''>(path: string, data?: any, config?: AxiosRequestConfig): Promise<AxiosSucceedResponse<T> | AxiosFailedResponse> => {
  const token = getCookie('csrftoken');
  let myConfig = { ...devConfig };
  if (token) {
    myConfig = {
      ...myConfig,
      headers: {
        'X-CSRFToken': token,
      },
    };
  }
  try {
    const response = await func<T>(HTTP_URL + path, data, { ...config, ...myConfig });
    return {
      ...response,
      state: 'success',
    };
  } catch (error) {
    if (error.response === undefined) {
      return {
        state: 'failed',
      };
    }

    return {
      ...error.response,
      state: 'failed',
    };
  }
};

export const rest = {
  get: wrapper1(Axios.get),
  delete: wrapper1(Axios.delete),

  post: wrapper2(Axios.post),
  put: wrapper2(Axios.put),
  patch: wrapper2(Axios.patch),
} as const;
