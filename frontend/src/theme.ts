import { createMuiTheme } from '@material-ui/core/styles';
// import purple from '@material-ui/core/colors/purple';
// import green from '@material-ui/core/colors/green';

const theme = createMuiTheme({
  palette: {
    type: 'dark',
    primary: {
      main: '#ea0042',
    },
    background: {
      default: '#100e19',
      paper: '#1f1b2e',
    },
  },
});

export default theme;
