import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router';
import { Link as RouterLink } from 'react-router-dom';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import { makeStyles } from '@material-ui/core/styles';
import Link from '@material-ui/core/Link';
import CircularProgress from '@material-ui/core/CircularProgress';
import PersonFilms from '../components/person/PersonFilms';
import { getPerson, Person, personFields } from '../api/person';

const useStyles = makeStyles((theme) => ({
  cardContentRoot: {
    display: 'flex',

    [theme.breakpoints.up('sm')]: {
      paddingLeft: 100,
    },
  },
  content: {
    fontSize: '2.5em',
  },
  cardContent: {
    display: 'flex',

    [theme.breakpoints.down('sm')]: {
      flexDirection: 'column',
      alignItems: 'center',
    },

    '& .cardMedia': {
      width: 300,
    },

    '& .cardInfo': {
      padding: 20,
      fontSize: '1.2rem',

      [theme.breakpoints.up('sm')]: {
        paddingLeft: 50,
      },

      '& .cardField': {
        display: 'flex',
        padding: '5px 0',

        borderBottom: '1px solid',
        borderBottomColor: '#d9d7e0',

        '& .title': {
          width: 120,
          minWidth: 120,

          color: '#d9d7e0',
        },

        '& .value': {
          flexGrow: 1,

          maxWidth: '20em',
        },
      },
    },
  },
  plot: {
    fontSize: '1.2em',
  },
  filmActions: {
    display: 'flex',

    [theme.breakpoints.down('sm')]: {
      justifyContent: 'space-around',
    },
  },
  loader: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: '80vh',
    overflow: 'hiiden',
  },
}));

const Film = () => {
  const { id } = useParams<{ id: string }>();
  const [loading, setLoading] = useState(false);
  const [person, setPerson] = useState<Person | null>(null);
  const classes = useStyles();

  useEffect(() => {
    const fetchPerson = async () => {
      setLoading(true);
      const res = await getPerson(Number(id));
      setLoading(false);
      if (res.state === 'success') {
        setPerson(res.data);
      }
    };
    fetchPerson();
  }, [id]);

  if (loading) {
    return (
      <div className={classes.loader}>
        <CircularProgress />
      </div>
    );
  }

  return (
    person === null ? (
      <Link component={RouterLink} to="/">
        Main
      </Link>
    ) : (
      <div>
        <Card>
          <CardHeader classes={{ title: classes.content }} title={person.name} />
          <CardContent className={classes.cardContentRoot}>
            <div>
              <div className={classes.cardContent}>
                <CardMedia className="cardMedia" component="img" alt={person.name} image={person.poster} />
                <div className="cardInfo">
                  {personFields.map((field) => (
                    <div className="cardField" key={field}>
                      <div className="title">
                        {field}
                      </div>
                      <div className="value">
                        {person[field] === 'N/A' || person[field] === '' ? '—' : person[field]}
                      </div>
                    </div>
                  ))}
                </div>
              </div>
            </div>
          </CardContent>
          <CardContent className={classes.plot}>
            {person.bio}
          </CardContent>
        </Card>
        <PersonFilms id={Number(id)} />
      </div>
    )
  );
};

export default Film;
