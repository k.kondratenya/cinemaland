import React from 'react';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { useHistory, useLocation, Link as RouterLink } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import Link from '@material-ui/core/Link';
import { yupResolver } from '@hookform/resolvers/yup';
import { object, string } from 'yup';
import { useAuth } from '../hooks/auth';
import { login as apiLogin } from '../api/auth';
// Исходник https://material-ui.com/ru/getting-started/templates/

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

type Form = {
  email: string;
  password: string;
}

type LocationState = {
  from: {
    pathname: string;
  };
}

const schema = object().shape({
  email: string().required().email(),
  password: string().required(),
});

export default function Login() {
  const classes = useStyles();
  const auth = useAuth();
  const location = useLocation<LocationState>();
  const history = useHistory();

  const { register, handleSubmit, errors } = useForm<Form>({
    resolver: yupResolver(schema),
    mode: 'onBlur',
  });

  const { from } = location.state || { from: { pathname: '/' } };
  const login = async (data: Form) => {
    const { email, password } = data;

    const res = await apiLogin({
      email,
      password,
    });

    if (res.state === 'success') {
      auth.signin({ ...res.data }, () => {
        history.replace(from);
      });
    }
  };

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Typography component="h1" variant="h5">
          Sign in
        </Typography>
        <form className={classes.form} noValidate onSubmit={handleSubmit(login)}>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            autoComplete="email"
            autoFocus
            error={!!(errors.email?.message)}
            helperText={errors.email?.message}
            inputRef={register}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
            error={!!errors.password?.message}
            helperText={errors.password?.message}
            inputRef={register}
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Sign In
          </Button>
          <Link component={RouterLink} className="flx-aic-jcc" color="primary" to="sign-up" variant="subtitle1">
            Sign up
          </Link>
        </form>
      </div>
    </Container>
  );
}
