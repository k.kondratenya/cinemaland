import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import { Link as RouterLink } from 'react-router-dom';
import Container from '@material-ui/core/Container';
import Link from '@material-ui/core/Link';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { object, string, ref } from 'yup';
import { signUp } from '../api/auth';
// Исходник https://material-ui.com/ru/getting-started/templates/

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

type Form = {
  email: string;
  username: string;
  password: string;
  confirmPassword: string;
}

const schema = object().shape({
  email: string().required().email(),
  username: string().required(),
  password: string().required().min(8),
  confirmPassword: string().oneOf([ref('password'), null], 'Field must be the same as password'),
});

export default function Login() {
  const classes = useStyles();

  const { register, handleSubmit, errors } = useForm<Form>({
    resolver: yupResolver(schema),
    mode: 'onBlur',
  });

  const login = async (data: Form) => {
    const {
      email, username, password, confirmPassword,
    } = data;

    await signUp({
      email,
      username,
      password,
      confirmPassword,
    });
  };

  return (
    <Container component="main" maxWidth="xs">
      <div className={classes.paper}>
        <Typography component="h1" variant="h5">
          Sign up
        </Typography>
        <form className={classes.form} noValidate onSubmit={handleSubmit(login)}>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            label="Email Address"
            name="email"
            autoComplete="email"
            autoFocus
            error={!!errors.email?.message}
            helperText={errors.email?.message}
            inputRef={register}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            label="Username"
            name="username"
            autoComplete="username"
            error={!!errors.username?.message}
            helperText={errors.username?.message}
            inputRef={register}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            error={!!errors.password?.message}
            helperText={errors.password?.message}
            autoComplete="current-password"
            inputRef={register}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="confirmPassword"
            label="Confirm password"
            type="password"
            error={!!errors.confirmPassword?.message}
            helperText={errors.confirmPassword?.message}
            autoComplete="current-password"
            inputRef={register}
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Sign Up
          </Button>
          <Link component={RouterLink} className="flx-aic-jcc" color="primary" to="login" variant="subtitle1">
            Log in
          </Link>
        </form>
      </div>
    </Container>
  );
}
