import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router';
import { Link as RouterLink } from 'react-router-dom';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import { makeStyles } from '@material-ui/core/styles';
import Link from '@material-ui/core/Link';
import CircularProgress from '@material-ui/core/CircularProgress';
import {
  FilmExtended, getFilm, filmFields, PartialFilmActions, FilmActor, FilmDirector,
} from '../api/films';
import Recommendation from '../components/Recommendation';
import Comments from '../components/comments/Comments';
import FilmActions from '../components/film/FilmActions';

const useStyles = makeStyles((theme) => ({
  cardContentRoot: {
    display: 'flex',

    [theme.breakpoints.up('sm')]: {
      paddingLeft: 100,
    },
  },
  content: {
    fontSize: '2.5em',
  },
  cardContent: {
    display: 'flex',

    [theme.breakpoints.down('sm')]: {
      flexDirection: 'column',
      alignItems: 'center',
    },

    '& .cardMedia': {
      width: 300,
    },

    '& .cardInfo': {
      padding: 20,
      fontSize: '1.2rem',

      [theme.breakpoints.up('sm')]: {
        paddingLeft: 50,
      },

      '& .cardField': {
        display: 'flex',
        padding: '5px 0',

        borderBottom: '1px solid',
        borderBottomColor: '#d9d7e0',

        '& .title': {
          width: 120,
          minWidth: 120,

          color: '#d9d7e0',
        },

        '& .value': {
          flexGrow: 1,

          maxWidth: '20em',
        },
      },
    },
  },
  plot: {
    fontSize: '1.2em',
  },
  filmActions: {
    display: 'flex',
    marginTop: '.5rem',

    [theme.breakpoints.down('sm')]: {
      justifyContent: 'space-around',
    },
  },
  loader: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: '80vh',
    overflow: 'hiiden',
  },
}));

const Film = () => {
  const { id } = useParams<{ id: string }>();
  const [film, setFilm] = useState<FilmExtended | null>(null);
  const [loading, setLoading] = useState(false);
  const classes = useStyles();

  useEffect(() => {
    const fetchFilms = async () => {
      setLoading(true);
      const res = await getFilm(Number(id));
      setLoading(false);

      if (res.state === 'success') {
        const { persons, ...partialFilm } = res.data;
        const actors: FilmActor[] = [];
        const directors: FilmDirector[] = [];

        persons.forEach((person) => {
          if (person.job === 'actor') {
            actors.push(person);
          } else {
            directors.push(person);
          }
        });

        setFilm({
          ...partialFilm,
          actors,
          directors,
        });
      }
    };
    fetchFilms();
  }, [id]);

  if (loading) {
    return (
      <div className={classes.loader}>
        <CircularProgress />
      </div>
    );
  }

  const actors = film?.actors.map((person, index) => (
    <span key={person.personID}>
      <Link component={RouterLink} color="primary" to={`/person/${person.personID}`}>
        { person.name }
      </Link>
      { index === film.actors.length - 1 ? '' : ',  ' }
    </span>
  ));

  const directors = film?.directors.map((person, index) => (
    <span key={person.personID}>
      <Link component={RouterLink} color="primary" to={`/person/${person.personID}`}>
        { person.name }
      </Link>
      { index === film.directors.length - 1 ? '' : ',  ' }
    </span>
  ));

  const updateFilm = (partialFields: PartialFilmActions) => {
    if (film === null) {
      return;
    }
    setFilm({ ...film, ...partialFields });
  };

  return (
    film === null ? (
      <Link component={RouterLink} to="/">
        Main
      </Link>
    ) : (
      <div>
        <Card>
          <CardHeader classes={{ title: classes.content }} title={film.title} />
          <CardContent className={classes.cardContentRoot}>
            <div>
              <div className={classes.cardContent}>
                <CardMedia className="cardMedia" component="img" alt={film.title} image={film.poster} />
                <div className="cardInfo">
                  <div className="cardField">
                    <div className="title">
                      directors
                    </div>
                    <div className="value">
                      {directors !== undefined && directors.length === 0 ? '-' : directors}
                    </div>
                  </div>
                  <div className="cardField">
                    <div className="title">
                      actors
                    </div>
                    <div className="value">
                      {actors !== undefined && actors.length === 0 ? '-' : actors}
                    </div>
                  </div>
                  {filmFields.map((field) => (
                    <div className="cardField" key={field}>
                      <div className="title">
                        {field}
                      </div>
                      <div className="value">
                        {film[field] === 'N/A' || film[field] === '' ? '—' : film[field]}
                      </div>
                    </div>
                  ))}
                  <FilmActions
                    className={classes.filmActions}
                    film={film}
                    updateFilm={updateFilm}
                  />
                </div>
              </div>
            </div>
          </CardContent>
          <CardContent className={classes.plot}>
            {film.plot}
          </CardContent>
        </Card>
        <Recommendation />
        <Comments id={Number(id)} />
      </div>
    )
  );
};

export default Film;
