import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from 'react-router-dom';
import './App.css';
import './styles/flx.css';
import { ThemeProvider } from '@material-ui/core/styles';
import AuthProvider from './components/auth/ProvideAuth';
import Login from './routes/Login';
import SignUp from './routes/SignUp';
import TopMenu from './layout/TopMenu';
import Main from './routes/Main';
import Film from './routes/Film';
import Person from './routes/Person';
import theme from './theme';

function App() {
  return (
    <ThemeProvider theme={theme}>
      <AuthProvider>
        <Router>
          <div className="App">
            <Switch>
              <TopMenu>
                <div>
                  <Route path="/login">
                    <Login />
                  </Route>
                  <Route path="/sign-up">
                    <SignUp />
                  </Route>
                  <Route path="/film/:id">
                    <Film />
                  </Route>
                  <Route path="/person/:id">
                    <Person />
                  </Route>
                  <Route exact path="/">
                    <Main />
                  </Route>
                </div>
              </TopMenu>
              <Route path="/">
                Not found
              </Route>
            </Switch>
          </div>
        </Router>
      </AuthProvider>
    </ThemeProvider>
  );
}

export default App;
