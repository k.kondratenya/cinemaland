import { useContext, useState, createContext } from 'react';

// исходник https://reactrouter.com/web/example/auth-workflow

export type User = {
  email: string;
  username: string;
  avatar: string;
}

export type UserOrAnonymous = User | null

type AuthCallback = () => void;

const fakeAuth = {
  signin(user: UserOrAnonymous, cb: AuthCallback) {
    setTimeout(cb, 100); // fake async
  },
  signout(cb: AuthCallback) {
    setTimeout(cb, 100);
  },
};

export function useProvideAuth() {
  const [user, setUser] = useState<UserOrAnonymous>(null);

  const signin = (userArgs: User, cb: AuthCallback) => {
    setUser(userArgs);
    if (cb) {
      cb();
    }
  };

  const signout = (cb: AuthCallback) => {
    setUser(null);
    if (cb) {
      cb();
    }
  };

  return {
    user,
    signin,
    signout,
  };
}

type AuthContextType = typeof fakeAuth & {
  user: UserOrAnonymous,
};

export const AuthContext = createContext<AuthContextType>({
  user: null,
  signin: (user: User, cb: AuthCallback) => cb && cb(),
  signout: (cb: AuthCallback) => cb && cb(),
});

export function useAuth() {
  return useContext(AuthContext);
}
