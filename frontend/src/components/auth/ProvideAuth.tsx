import PropTypes from 'prop-types';
import React, { ReactElement, useEffect } from 'react';
import { getUser } from '../../api/auth';
import { useProvideAuth, AuthContext } from '../../hooks/auth';

interface Props {
  children: ReactElement | ReactElement[];
}

const ProvideAuth = ({ children }: Props) => {
  const auth = useProvideAuth();
  useEffect(() => {
    const fetchUser = async () => {
      const res = await getUser();
      if (res.state === 'success' && res.data) {
        auth.signin({ ...res.data }, () => {});
      }
    };
    fetchUser();
  }, []);
  return (
    <AuthContext.Provider value={auth}>
      {children}
    </AuthContext.Provider>
  );
};

ProvideAuth.propTypes = {
  children: PropTypes.element.isRequired,
};

export default ProvideAuth;
