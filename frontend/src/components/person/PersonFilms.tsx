import React, { useEffect, useState, FC } from 'react';
import FilmsCarousel from '../film/FilmsCarousel';
import { Film, PartialFilmActions } from '../../api/films';
import { getPersonFilms } from '../../api/person';

interface Props {
  id: number;
}

const PersonFilms: FC<Props> = ({ id }) => {
  const [films, setFilms] = useState<Film[]>([]);

  const updateFilm = (updateID: number, partialFields: PartialFilmActions) => {
    setFilms(films.map((film) => (film.id === updateID ? { ...film, ...partialFields } : film)));
  };

  useEffect(() => {
    const fetchFilms = async () => {
      const res = await getPersonFilms(id);
      if (res.state === 'success') {
        setFilms(res.data);
      }
    };
    fetchFilms();
  }, []);

  return (
    <FilmsCarousel newFilms={films} maxWidth={170} updateFilm={updateFilm} />
  );
};

export default PersonFilms;
