import React, {
  useRef, useLayoutEffect, ReactElement, FC,
} from 'react';
import ArrowBack from '@material-ui/icons/ArrowBack';
import ArrowForward from '@material-ui/icons/ArrowForward';
import { makeStyles } from '@material-ui/core/styles';
import debounce from 'lodash.debounce';
import { IconButton } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    alignItems: 'center',

    padding: '20px 0',
  },
  inner: {
    display: 'flex',
    position: 'relative',
    flex: 1,
    overflowX: 'auto',
    '-ms-overflow-style': 'none', /* IE and Edge */
    scrollbarWidth: 'none',

    '&::-webkit-scrollbar': {
      display: 'none',
    },

    [theme.breakpoints.up('sm')]: {
      overflowX: 'hidden',
    },
  },
  carousel: {
    display: 'flex',
  },
  button: {
    [theme.breakpoints.down('sm')]: {
      display: 'none',
    },
  },
}));

type Props = {
  children: ReactElement[];
  maxWidth: number;
  setWidth: Function;
}

const FilmsCarousel: FC<Props> = ({ children, maxWidth, setWidth }) => {
  const classes = useStyles();
  const inner = useRef<HTMLDivElement>(null);

  const calculateWidth = () => {
    if (inner.current === null) {
      return;
    }
    const numberOfElements = Math.ceil(inner.current.clientWidth / maxWidth);
    if (inner === null || inner.current === null) {
      return;
    }
    setWidth((inner.current.clientWidth / numberOfElements));
  };

  const debounceCalculateWidth = debounce(calculateWidth, 500);

  useLayoutEffect(() => {
    calculateWidth();
    const resizeObserver = new ResizeObserver(debounceCalculateWidth);
    if (inner.current === null) {
      return;
    }
    resizeObserver.observe(inner.current);

    return () => {
      debounceCalculateWidth.cancel();
      resizeObserver.disconnect();
    };
  }, []);

  const scroll = (forward: boolean) => () => {
    if (inner.current === null) {
      return;
    }
    if (forward) {
      inner.current.scrollBy({ top: 0, left: inner.current.clientWidth, behavior: 'smooth' });
    } else {
      inner.current.scrollBy({ top: 0, left: -inner.current.clientWidth, behavior: 'smooth' });
    }
  };

  return (
    <div className={classes.root}>
      <IconButton className={classes.button} onClick={scroll(false)}>
        <ArrowBack />
      </IconButton>
      <div ref={inner} className={classes.inner}>
        <div className={classes.carousel}>
          {children}
        </div>
      </div>
      <IconButton className={classes.button} onClick={scroll(true)}>
        <ArrowForward />
      </IconButton>
    </div>
  );
};

export default FilmsCarousel;
