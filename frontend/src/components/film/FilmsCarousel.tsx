import React, { useState } from 'react';
import FilmCard from './FilmCard';
import Carousel from '../Carousel';
import { Film } from '../../api/films';

type Props = {
  newFilms: Film[],
  maxWidth?: number,
  updateFilm: Function,
}

const FilmsCarousel: React.FC<Props> = ({ newFilms, maxWidth = 250, updateFilm }) => {
  const [width, setWidth] = useState(maxWidth);

  const items = [];

  if (newFilms.length !== 0) {
    items.push(...newFilms.map((film) => (
      <FilmCard key={film.id} film={film} width={width} updateFilm={updateFilm} />
    )));
    console.log(items);
  } else {
    for (let i = 0; i < 15; i += 1) {
      items.push(<FilmCard key={i} film={null} width={width} />);
    }
    console.log(items);
  }

  return (
    <Carousel maxWidth={maxWidth} setWidth={setWidth}>
      {items}
    </Carousel>
  );
};

export default FilmsCarousel;
