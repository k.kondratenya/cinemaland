import React from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox, { CheckboxProps } from '@material-ui/core/Checkbox';
import Favorite from '@material-ui/icons/Favorite';
import FavoriteBorder from '@material-ui/icons/FavoriteBorder';
import BookMark from '@material-ui/icons/Bookmark';
import BookMarkBorder from '@material-ui/icons/BookmarkBorder';
import Link from '@material-ui/core/Link';
import { Link as RouterLink } from 'react-router-dom';
import { Film, setFilmAction } from '../../api/films';
import { useAuth } from '../../hooks/auth';

const WhiteCheckbox = withStyles({
  root: {
    color: 'white',
  },
  checked: {},
// eslint-disable-next-line react/jsx-props-no-spreading
})((props: CheckboxProps) => <Checkbox color="default" {...props} />);

const useStyles = makeStyles((theme) => ({
  root: ({ width }: { width: number }) => ({
    position: 'relative',
    width,
    padding: '0 12px 5px',

    '&:hover .cardDescription': {
      display: 'block',
      opacity: 1,
    },
  }),
  link: {
    textDecoration: 'none',
    color: '#d9d7e0',
    fontWeight: 600,
  },
  cardMedia: ({ width } : { width: number }) => ({
    height: width * (4 / 3),
  }),
  skeleton: ({ width } : { width: number }) => ({
    height: width * (4 / 3),
    width: width - 12,
    background: theme.palette.background.paper,
    margin: '0 12px 5px',
  }),
  cardContent: {
    display: '-webkit-box',
    '-webkit-line-clamp': 2,
    '-webkit-box-orient': 'vertical',
    height: '2.5rem',
    padding: '0',
    margin: 10,
    overflow: 'hidden',
    'overflow-wrap': 'break-word',
    'text-overflow': 'ellipsis',
  },
  cardControls: {
    position: 'absolute',
    top: 0,
    right: 0,
  },
  cardActionArea: {
    position: 'relative',
  },
  cardDescription: {
    position: 'absolute',
    top: 0,
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0, 0, 0, 30%)',
    display: 'none',
    opacity: 0,

    '& > .info': {
      display: 'flex',
      'align-items': 'center',
      height: '100%',
      padding: '0 10px',

      color: 'white',
    },
  },
  checkboxItem: {
    margin: 0,
  },
}));

type Props = {
  film: Film | null,
  width: number,
  updateFilm?: Function,
}

const FilmCard: React.FC<Props> = ({ film, width, updateFilm = () => {} }) => {
  const classes = useStyles({ width });
  const { user } = useAuth();

  if (film === null) {
    return (<div className={classes.skeleton} />);
  }

  const setLike = async () => {
    const res = await setFilmAction(film.id, { isFavorite: !film.isFavorite });
    if (res.state === 'success') {
      updateFilm(film.id, { isFavorite: !film.isFavorite });
    }
  };

  const setWatchLater = async () => {
    const res = await setFilmAction(film.id, { watchLater: !film.watchLater });
    if (res.state === 'success') {
      updateFilm(film.id, { watchLater: !film.watchLater });
    }
  };

  return (
    <div className={classes.root}>
      <Card>
        <Link component={RouterLink} color="primary" to={`/film/${film.id}`} className={classes.link}>
          <div className={classes.cardActionArea}>
            <CardMedia className={classes.cardMedia} component="img" alt={film.title} image={film.poster} />
            <div className={`${classes.cardDescription} cardDescription`}>
              <div className="info">
                <div>
                  <div>
                    {film.title}
                  </div>
                  <div>
                    {film.year}
                  </div>
                </div>
              </div>
              {user && (
              <FormGroup
                className={classes.cardControls}
                onClick={(event) => {
                  event.stopPropagation();
                }}
              >
                <FormControlLabel
                  label=""
                  className={classes.checkboxItem}
                  control={(
                    <WhiteCheckbox
                      icon={(<FavoriteBorder />)}
                      checkedIcon={<Favorite />}
                    />
                )}
                  checked={!!film.isFavorite}
                  onClick={setLike}
                />
                <FormControlLabel
                  label=""
                  className={classes.checkboxItem}
                  control={(
                    <WhiteCheckbox
                      icon={<BookMarkBorder />}
                      checkedIcon={<BookMark />}
                    />
                )}
                  checked={!!film.watchLater}
                  onClick={setWatchLater}
                />
              </FormGroup>
              )}
            </div>
          </div>
          <CardContent className={classes.cardContent}>
            { film.title }
          </CardContent>
        </Link>
      </Card>
    </div>
  );
};

export default FilmCard;
