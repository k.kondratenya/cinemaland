import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox, { CheckboxProps } from '@material-ui/core/Checkbox';
import Favorite from '@material-ui/icons/Favorite';
import FavoriteBorder from '@material-ui/icons/FavoriteBorder';
import BookMark from '@material-ui/icons/Bookmark';
import BookMarkBorder from '@material-ui/icons/BookmarkBorder';
import StarBorder from '@material-ui/icons/StarBorder';
import Star from '@material-ui/icons/Star';
import VisibilityBorder from '@material-ui/icons/VisibilityOutlined';
import Visibility from '@material-ui/icons/Visibility';
import Popover from '@material-ui/core/Popover';
import Rating from '@material-ui/lab/Rating';
import { FilmExtended, setFilmAction, setFilmRate } from '../../api/films';

const WhiteCheckbox = withStyles({
  root: {
    color: 'white',
  },
  checked: {},
// eslint-disable-next-line react/jsx-props-no-spreading
})((props: CheckboxProps) => <Checkbox color="default" {...props} />);

type Props = {
  film: FilmExtended,
  updateFilm: Function,
  className?: string,
}

const FilmActions: React.FC<Props> = ({ film, updateFilm, className }) => {
  const [anchorEl, setAnchorEl] = React.useState<HTMLLabelElement | null>(null);
  const [open, setOpen] = React.useState(false);

  const handleStar = (event: React.MouseEvent<HTMLLabelElement>) => {
    setAnchorEl(event.currentTarget);
    setOpen(true);
  };

  const closePopover = () => {
    setAnchorEl(null);
    setOpen(false);
  };

  const setLike = async () => {
    const res = await setFilmAction(film.id, { isFavorite: !film.isFavorite });
    if (res.state === 'success') {
      updateFilm({ isFavorite: !film.isFavorite });
    }
  };

  const setWatchLater = async () => {
    const res = await setFilmAction(film.id, { watchLater: !film.watchLater });
    if (res.state === 'success') {
      updateFilm({ watchLater: !film.watchLater });
    }
  };

  const setWatched = async () => {
    const res = await setFilmRate(film.id, { watched: !film.watched });
    if (res.state === 'success') {
      updateFilm({ ...res.data });
    }
  };

  const setRating = async (event: React.ChangeEvent<{}>, newValue: number | null) => {
    if (newValue === null) {
      return;
    }

    const res = await setFilmRate(film.id, { userRating: newValue });
    if (res.state === 'success') {
      updateFilm({ ...res.data });
    }
  };

  return (
    <FormGroup
      row
      className={className}
      onClick={(event) => {
        event.stopPropagation();
      }}
    >
      <FormControlLabel
        label="Favorite"
        control={(
          <WhiteCheckbox
            icon={(<FavoriteBorder />)}
            checkedIcon={<Favorite />}
          />
        )}
        checked={!!film.isFavorite}
        onClick={setLike}
      />
      <FormControlLabel
        label="Watch later"
        control={(
          <WhiteCheckbox
            icon={<BookMarkBorder />}
            checkedIcon={<BookMark />}
          />
        )}
        checked={!!film.watchLater}
        onClick={setWatchLater}
      />
      <FormControlLabel
        label="Watched"
        control={(
          <WhiteCheckbox
            icon={<VisibilityBorder />}
            checkedIcon={<Visibility />}
          />
        )}
        checked={!!film.watched}
        onClick={setWatched}
      />
      <FormControlLabel
        label={film.userRating === null ? 'Set rating' : `My rating ${film.userRating}`}
        control={(
          <WhiteCheckbox
            icon={<StarBorder />}
            checkedIcon={<Star />}
          />
        )}
        checked={!!(film.userRating !== null)}
        onClick={handleStar}
      />
      <Popover
        open={open}
        anchorEl={anchorEl}
        anchorOrigin={{
          vertical: 'center',
          horizontal: 'right',
        }}
        transformOrigin={{
          vertical: 'center',
          horizontal: 'left',
        }}
        onClose={closePopover}
      >
        <Rating
          value={film.userRating}
          name="customized-10"
          max={10}
          size="medium"
          onChange={setRating}
        />
      </Popover>
    </FormGroup>
  );
};

export default FilmActions;
