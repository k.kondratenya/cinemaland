import React, { FC } from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Avatar from '@material-ui/core/Avatar';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import ThumbDownIcon from '@material-ui/icons/ThumbDown';
import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import ThumbUpAltOutlinedIcon from '@material-ui/icons/ThumbUpAltOutlined';
import ThumbDownAltOutlinedIcon from '@material-ui/icons/ThumbDownAltOutlined';
import Checkbox, { CheckboxProps } from '@material-ui/core/Checkbox';
import { Comment, postCommentRate, Rate } from '../../api/films';

const RedCheckbox = withStyles({
  root: {
    color: 'red',
  },
  checked: {},
// eslint-disable-next-line react/jsx-props-no-spreading
})((props: CheckboxProps) => <Checkbox color="default" {...props} />);

const GreenCheckbox = withStyles({
  root: {
    color: 'green',
  },
  checked: {},
// eslint-disable-next-line react/jsx-props-no-spreading
})((props: CheckboxProps) => <Checkbox color="default" {...props} />);

const useStyles = makeStyles({
  root: {
    padding: 5,
    margin: '10px 0',
  },
  cardContent: {
    display: 'flex',

    '& .comment': {
      display: 'flex',
      flexDirection: 'column',
      width: '100%',
      padding: '0 15px',

      '& .comment-header': {
        display: 'flex',
        justifyContent: 'space-between',
      },

      '& .comment-header__nick': {
        fontSize: '1.25em',
        fontWeight: 700,
      },

      '& .comment__inner': {
        whiteSpace: 'pre-line',
        wordBreak: 'break-word',
        paddingTop: 5,
      },

      '& .comment-header__counter': {
        minWidth: '2em',
        textAlign: 'center',
        fontSize: '1.1em',
        fontWeight: 700,
        padding: '0 5px',
      },
    },
  },
  checkboxItem: {
    marginRight: 0,
    marginLeft: 0,
  },
});

type Props = {
  comment: Comment,
  updateCommentRate: Function,
}

const Comments: FC<Props> = ({ comment, updateCommentRate }) => {
  const classes = useStyles();

  const onClick = (newRate: Rate) => async () => {
    const rate = newRate === comment.rate ? 0 : newRate;

    const res = await postCommentRate(comment.id, { rate });
    if (res.state === 'success') {
      updateCommentRate(comment.id, rate);
    }
  };

  return (
    <Card className={classes.root} variant="outlined">
      <CardContent className={classes.cardContent}>
        <Avatar>{comment.username[0]}</Avatar>
        <div className="comment">
          <div className="comment-header">
            <div className="comment-header__nick">
              {comment.username}
            </div>
          </div>
          <div className="comment__inner">
            {comment.content}
          </div>
          <FormGroup
            row
            className="flx-jcfe"
            onClick={(event) => {
              event.stopPropagation();
            }}
          >
            <FormControlLabel
              label=""
              className={classes.checkboxItem}
              control={(
                <GreenCheckbox
                  icon={<ThumbUpAltOutlinedIcon />}
                  checkedIcon={<ThumbUpIcon />}
                />
                )}
              checked={comment.rate === 1}
              onClick={onClick(1)}
            />
            <span className="flx-aic-jcc comment-header__counter">
              {comment.sumRate}
            </span>
            <FormControlLabel
              label=""
              className={classes.checkboxItem}
              control={(
                <RedCheckbox
                  icon={<ThumbDownAltOutlinedIcon />}
                  checkedIcon={<ThumbDownIcon />}
                />
                )}
              checked={comment.rate === -1}
              onClick={onClick(-1)}
            />
          </FormGroup>
        </div>
      </CardContent>
    </Card>
  );
};

export default Comments;
