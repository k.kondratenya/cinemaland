import React, { useEffect, useState } from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/icons/Send';
import { makeStyles } from '@material-ui/core/styles';
import CommentContainer from './CommentContainer';
import {
  Comment, getFilmComments, postFilmComments, Rate,
} from '../../api/films';

const useStyles = makeStyles({
  root: {
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
    maxWidth: 900,
    margin: 'auto',
    padding: 5,
  },
  textField: {
    color: '#efeff3',
  },
  buttonWrapper: {
    display: 'flex',
    justifyContent: 'flex-end',
    margin: '10px 0',
  },
});

type Props = {
  id: number;
}

const Comments: React.FC<Props> = ({ id }) => {
  const classes = useStyles();
  const [comments, setComments] = useState<Comment[]>([]);
  const [value, setValue] = useState('');

  useEffect(() => {
    const fetchComments = async () => {
      const res = await getFilmComments(id);

      if (res.state === 'success') {
        setComments(res.data);
      }
    };
    fetchComments();
  }, [id]);

  const addComment = async () => {
    if (value === '') {
      return;
    }

    const res = await postFilmComments(id, { content: value.replace(/(^[ \t]*\n+)/gm, '\n').trim() });

    if (res.state === 'success') {
      setComments([
        res.data,
        ...comments,
      ]);
    }
    setValue('');
  };

  const updateCommentRate = (commentID: number, rate: Rate) => {
    setComments(
      comments.map((comment) => (comment.id
        === commentID
        ? { ...comment, sumRate: comment.sumRate - comment.rate + rate, rate } : comment)),
    );
  };

  const handleInput = (event: React.ChangeEvent<HTMLInputElement>) => {
    setValue(event.target.value);
  };

  return (
    <div className={classes.root}>
      <div>
        <TextField
          label="Leave your comment"
          multiline
          variant="outlined"
          fullWidth
          rows={3}
          rowsMax={6}
          value={value}
          onInput={handleInput}
        />
        <div className={classes.buttonWrapper}>
          <Button
            variant="contained"
            color="primary"
            endIcon={<Icon>send</Icon>}
            onClick={addComment}
          >
            Send
          </Button>
        </div>
      </div>
      <div>
        {comments.map((comment) => (
          <CommentContainer
            comment={comment}
            key={comment.id}
            updateCommentRate={updateCommentRate}
          />
        ))}
      </div>
    </div>
  );
};

export default Comments;
