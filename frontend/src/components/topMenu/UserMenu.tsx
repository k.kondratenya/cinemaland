import React, { MouseEvent, useState } from 'react';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';
import { Link } from 'react-router-dom';
import IconButton from '@material-ui/core/IconButton';
import { useAuth, User } from '../../hooks/auth';
import UserSettings from './UserSettings';
import { logout } from '../../api/auth';

function UserMenu() {
  const { user, signout, signin } = useAuth();
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const [isSettingsOpened, setSettingsOpened] = useState(false);
  const open = Boolean(anchorEl);

  const handleMenu = (event: MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const updateUser = (newUser: User) => {
    signin(newUser, () => {});
  };

  const handleSettingsOpen = () => {
    setAnchorEl(null);
    setSettingsOpened(true);
  };

  const handleSettingsClose = () => {
    setSettingsOpened(false);
  };

  const handleSignout = async () => {
    setAnchorEl(null);
    const res = await logout();
    if (res.state === 'success') {
      signout(() => {});
    }
  };

  if (user === null) {
    return (
      <div>
        <Button component={Link} to="/login" color="inherit">Log in</Button>
      </div>
    );
  }

  return (
    <div>
      <IconButton
        aria-label="account of current user"
        aria-controls="menu-appbar"
        aria-haspopup
        onClick={handleMenu}
        color="inherit"
      >
        <AccountCircle />
      </IconButton>
      <Menu
        id="menu-appbar"
        elevation={0}
        anchorEl={anchorEl}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        transformOrigin={{
          vertical: 'bottom',
          horizontal: 'right',
        }}
        open={open}
        onClose={handleClose}
      >
        <MenuItem onClick={handleSettingsOpen}>Edit profile</MenuItem>
        <MenuItem onClick={handleSignout}>Logout</MenuItem>
      </Menu>
      {isSettingsOpened
      && <UserSettings user={user} updateUser={updateUser} onClose={handleSettingsClose} />}
    </div>
  );
}

export default UserMenu;
