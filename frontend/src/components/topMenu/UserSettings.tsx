import React, { FC } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { makeStyles } from '@material-ui/core/styles';
import { object, string } from 'yup';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { User } from '../../hooks/auth';
import { updateUser as updateUserApi } from '../../api/auth';

const useStyles = makeStyles({
  root: {
    width: 450,
    maxWidth: '100%',
  },
  avatar: {
    width: '12rem',
    height: '12rem',
    fontSize: '8rem',
  },
  email: {
    margin: '1rem 0',
  },
});

const schema = object().shape({
  email: string().required().email(),
  username: string().required(),
});

type Props = {
  user: User;
  onClose: () => void;
  updateUser: Function;
}

type Form = {
  username: string;
  email: string;
}

const UserSettings: FC<Props> = ({ user, onClose, updateUser }) => {
  const classes = useStyles();

  const {
    register, handleSubmit, errors,
  } = useForm<Form>({
    resolver: yupResolver(schema),
    mode: 'onBlur',
  });

  const submit = async (data: Form) => {
    const res = await updateUserApi(data);

    if (res.state === 'success') {
      updateUser(data);
      onClose();
    }
  };

  return (
    <Dialog open onClose={onClose} aria-labelledby="form-dialog-title">
      <DialogTitle id="form-dialog-title">Edit profile</DialogTitle>
      <form className={classes.root} onSubmit={handleSubmit(submit)}>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            name="email"
            defaultValue={user.email}
            label="Email"
            type="email"
            fullWidth
            error={!!(errors.email?.message)}
            helperText={errors.email?.message}
            inputRef={register}
          />
          <TextField
            margin="dense"
            name="username"
            defaultValue={user.username}
            label="Username"
            type="text"
            fullWidth
            error={!!(errors.username?.message)}
            helperText={errors.username?.message}
            inputRef={register}
          />
        </DialogContent>
        <DialogActions>
          <Button type="button" onClick={onClose} color="primary">
            Cancel
          </Button>
          <Button type="submit" color="primary">
            Save
          </Button>
        </DialogActions>
      </form>
    </Dialog>
  );
};

export default UserSettings;
