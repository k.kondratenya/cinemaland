import InputBase from '@material-ui/core/InputBase';
import { fade, makeStyles } from '@material-ui/core/styles';
import SearchIcon from '@material-ui/icons/Search';
import React, { useState, useEffect } from 'react';
import Autocomplete from '@material-ui/lab/Autocomplete';
import debounce from 'lodash.debounce';
import { useHistory } from 'react-router-dom';
import { Film, filmSearch } from '../../api/films';
import SearchResult from './SearchResult';

const useStyles = makeStyles((theme) => ({
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginLeft: 0,
    width: '100%',
    height: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(1),
      width: 'auto',
    },
  },
  searchIcon: {
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputRoot: {
    color: 'inherit',
    width: '100%',
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon,
    paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: '40ch',
    },
  },
  listbox: {
    maxHeight: '80vh',
  },
}));

export default function Search() {
  const classes = useStyles();

  const history = useHistory();

  const [open, setOpen] = React.useState(false);
  const [inputValue, setInputValue] = React.useState('');
  const [options, setOptions] = React.useState<Film[]>([]);

  const search = async (newValue: string) => {
    const res = await filmSearch(newValue);

    if (res.state === 'success') {
      setOptions(res.data);
    }
  };

  const [searchDebounce] = useState(() => debounce(search, 500));

  useEffect(() => {
    if (inputValue === '') {
      searchDebounce.cancel();
      return;
    }

    searchDebounce(inputValue);
  }, [inputValue]);

  const handleChange = (event: unknown, film: Film | null) => {
    if (film === null) {
      return;
    }

    setOptions([]);
    setInputValue('');
    history.push(`/film/${film.id}`);
  };

  return (
    <Autocomplete
      open={open}
      onOpen={() => {
        setOpen(true);
      }}
      onClose={() => {
        setOpen(false);
      }}
      getOptionSelected={(option, newOption) => option.id === newOption.id}
      getOptionLabel={(option) => option.title}
      inputValue={inputValue}
      onInputChange={(_event, newInputValue) => {
        setInputValue(newInputValue);
      }}
      classes={{
        listbox: classes.listbox,
      }}
      onChange={handleChange}
      options={options}
      renderInput={(params) => (
        <div className={classes.search}>
          <div className={classes.searchIcon}>
            <SearchIcon />
          </div>
          <InputBase
            placeholder="Search…"
            ref={params.InputProps.ref}
            classes={{
              root: classes.inputRoot,
              input: classes.inputInput,
            }}
            inputProps={params.inputProps}
          />
        </div>
      )}
      renderOption={(option) => (
        <SearchResult film={option} />
      )}
    />
  );
}
