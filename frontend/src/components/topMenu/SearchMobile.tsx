import React, { useEffect, useState } from 'react';
import Modal from '@material-ui/core/Modal';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';
import CloseIcon from '@material-ui/icons/Close';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import { useLocation } from 'react-router-dom';
import Search from './SearchField';

const useStyles = makeStyles((theme) => ({
  modalInner: {
    padding: '10em',
    background: theme.palette.background.default,
    width: '100%',
    height: '100%',

    [theme.breakpoints.down('sm')]: {
      padding: '1em',
    },
  },
}));

export default function SearchMobile() {
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  const location = useLocation();

  useEffect(() => {
    setOpen(false);
  }, [location]);

  return (
    <div>
      <IconButton onClick={() => setOpen(true)}>
        <SearchIcon />
      </IconButton>
      <Modal open={open} onClose={() => setOpen(false)}>
        <div className={classes.modalInner}>
          <Grid container justify="flex-end">
            <IconButton onClick={() => setOpen(false)}>
              <CloseIcon />
            </IconButton>
          </Grid>
          <Search />
        </div>
      </Modal>
    </div>
  );
}
