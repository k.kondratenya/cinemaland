import React, { FC } from 'react';
import withWidth, { isWidthUp, WithWidth } from '@material-ui/core/withWidth';
import SearchField from './SearchField';
import SearchMobile from './SearchMobile';

type Props = WithWidth

const Search: FC<Props> = ({ width }) => {
  if (isWidthUp('sm', width)) {
    return <SearchField />;
  }

  return <SearchMobile />;
};

export default withWidth()(Search);
