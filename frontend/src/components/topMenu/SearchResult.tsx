import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
// import Card from '@material-ui/core/Card';
import React, { FC } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Film } from '../../api/films';

const useStyles = makeStyles({
  cardRoot: {
    display: 'flex',
    width: '100%',
    height: '100%',
  },
  cardMediaRoot: {
    width: 70,
  },
});

interface Props {
  film: Film;
}

const SearchResult: FC<Props> = ({ film }) => {
  const classes = useStyles();
  return (
    <div className={classes.cardRoot}>
      <CardMedia className={classes.cardMediaRoot} component="img" alt={film.title} image={film.poster} />
      <CardContent>
        <div>
          {film.title}
        </div>
        <div>
          {film.year}
        </div>
      </CardContent>
    </div>
  );
};

export default SearchResult;
