import React, { useEffect, useState } from 'react';
import FilmsCarousel from './film/FilmsCarousel';
import { Film, getFilms, PartialFilmActions } from '../api/films';

function Main() {
  const [films, setFilms] = useState<Film[]>([]);

  const updateFilm = (id: number, partialFields: PartialFilmActions) => {
    setFilms(films.map((film) => (film.id === id ? { ...film, ...partialFields } : film)));
  };

  useEffect(() => {
    const fetchFilms = async () => {
      const res = await getFilms();
      if (res.state === 'success') {
        setFilms(res.data);
      }
    };
    fetchFilms();
  }, []);

  return (
    <FilmsCarousel newFilms={films} maxWidth={170} updateFilm={updateFilm} />
  );
}

export default Main;
