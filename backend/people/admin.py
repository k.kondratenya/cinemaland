from django.contrib import admin

from .models import Person, PersonWithFilm


class PersonAdmin(admin.ModelAdmin):
    list_display = ('name', 'imdbID')


# Register your models here.
admin.site.register(Person, PersonAdmin)

class PersonWithFilmAdmin(admin.ModelAdmin):
    list_display = ('film', 'person')


# Register your models here.
admin.site.register(PersonWithFilm, PersonWithFilmAdmin)
