from django.db import models
from user.models import User
from film.models import Film

class Person(models.Model):

    imdbID = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    bio = models.TextField()
    birthDetails = models.CharField(max_length=255)
    deathDetails = models.CharField(max_length=255)
    poster = models.CharField(verbose_name='Poster', max_length=255)


    class Meta:
        db_table = 'Person'
        verbose_name = 'Person'


class PersonWithFilm(models.Model):

    person = models.ForeignKey(Person, related_name='person_films',  on_delete=models.CASCADE)
    film = models.ForeignKey(Film, related_name='film_persons', on_delete=models.CASCADE)
    job = models.CharField(max_length = 20)
    role = models.CharField(max_length = 20, null=True)

    class Meta:
        db_table = 'PersonWithFilm'
        verbose_name = 'PersonWithFilm'
