# from django.contrib import admin
from django.urls import path

from .views import init, init_job, person, person_films

urlpatterns = [
    # path('', films),
    path('<int:id>/', person),
    path('<int:id>/films/', person_films),
    # path('<int:id>/rate/', film_rating),
    path('init/', init),
    path('init_job/', init_job),
]
