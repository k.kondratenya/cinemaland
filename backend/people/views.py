from django.core.handlers.wsgi import WSGIRequest
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.response import Response
from rest_framework.status import (
    HTTP_200_OK, HTTP_201_CREATED, HTTP_204_NO_CONTENT,
    HTTP_400_BAD_REQUEST, HTTP_403_FORBIDDEN, HTTP_404_NOT_FOUND, HTTP_405_METHOD_NOT_ALLOWED,
    HTTP_503_SERVICE_UNAVAILABLE,
)
from django.conf import settings
from django.core.paginator import Paginator
from django.core import serializers
from django.forms.models import model_to_dict
from django.db.models import Prefetch
import csv
import os
from .serializer import PersonSerializer
from film.serializer import FilmResponseSmall
from .models import Person, PersonWithFilm
from film.models import Film, FilmActions
from django.shortcuts import get_object_or_404
from drf_yasg.utils import swagger_auto_schema

@swagger_auto_schema(method='get', responses={200: PersonSerializer})
@api_view(['GET'])
def person(request: WSGIRequest, id) -> Response:
    person = get_object_or_404(Person, pk=id)
    person_dict = model_to_dict(person)

    return Response(person_dict)

@swagger_auto_schema(method='get', responses={200: FilmResponseSmall})
@api_view(['GET'])
def person_films(request: WSGIRequest, id) -> Response:
    person = get_object_or_404(Person, pk=id)
    films_ids = list(person.person_films.values_list('film__id', flat=True))

    print(films_ids)

    films = Film.objects.filter(id__in=films_ids).all()

    if (not request.user.is_authenticated):
        films_result = films.values()
    else:
        films = films.prefetch_related(Prefetch('film_actions', queryset=FilmActions.objects.filter(user=request.user))).all()
        films_result = []
        for film in films:
            film_dict = model_to_dict(film)
            film_actions = film.film_actions.values('isFavorite', 'watchLater')
            default_dict = {
                'isFavorite': False,
                'watchLater': False,
            }
            film_dict.update(film_actions[0] if not len(film_actions) == 0 else default_dict)
            films_result.append(film_dict)

    return Response(films_result)

@swagger_auto_schema(method='get', auto_schema=None)
@api_view(['GET'])
@permission_classes([IsAdminUser])
def init(request: WSGIRequest) -> Response:

    the_file = (str(settings.BASE_DIR) + '/people/names2.csv')
    filename = os.path.basename(os.path.join(str(settings.BASE_DIR), '/people/names.csv'))
    with open(the_file) as obj:
        reader = csv.reader(obj, delimiter=",")
        data = list(reader)
        for person in data[1:4001]:
            imdb_id = person[0]
            name = person[1]
            bio = person[4]
            birth_details = person[5]
            death_details = person[8]
            poster = person[-1]

            person_db = Person(
                imdbID=imdb_id,
                name=name,
                bio=bio,
                birthDetails=birth_details,
                deathDetails=death_details,
                poster=poster,
            )
            try:
                person_db.save()
            except:
                continue

    return Response()

@swagger_auto_schema(method='get', auto_schema=None)
@api_view(['GET'])
@permission_classes([IsAdminUser])
def init_job(request: WSGIRequest) -> Response:

    the_file = (str(settings.BASE_DIR) + '/people/principals2.csv')
    filename = os.path.basename(os.path.join(str(settings.BASE_DIR), '/people/names.csv'))
    with open(the_file) as obj:
        reader = csv.reader(obj, delimiter=",")
        data = list(reader)
        i = 0
        for principal in data[128640:]:
            i += 1
            film_id = principal[0]
            person_id = principal[2]
            job = principal[3]
            role = principal[4]

            try:
                person = Person.objects.get(imdbID=person_id)
                film = Film.objects.get(imdbID=film_id)
            except:
                print(i)
                continue

            person_db = PersonWithFilm(
                person=person,
                film=film,
                job=job,
                role=role if len(role) > 0 else None,
            )
            try:
                person_db.save()
            except:
                continue
    return Response()
