from django.shortcuts import render
from django.http import JsonResponse
from django.contrib.auth import authenticate, login, logout
# from django.contrib.sites.shortcuts import get_current_site
from django.core.handlers.wsgi import WSGIRequest
from django.utils.http import urlsafe_base64_decode, urlsafe_base64_encode
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.status import (
    HTTP_200_OK, HTTP_201_CREATED, HTTP_204_NO_CONTENT,
    HTTP_400_BAD_REQUEST, HTTP_403_FORBIDDEN, HTTP_404_NOT_FOUND, HTTP_405_METHOD_NOT_ALLOWED,
    HTTP_503_SERVICE_UNAVAILABLE,
)

from .models import User
from .serializer import UserSerializer, UserSmallSerializer, UserLoginSerializer
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi

# Create your views here.

@swagger_auto_schema(method='post', request_body=UserLoginSerializer, responses={200: UserSmallSerializer})
@api_view(['POST'])
def login_user(request: WSGIRequest) -> Response:
    if request.user.is_authenticated:
        return Response(status=HTTP_405_METHOD_NOT_ALLOWED)

    user = authenticate(request, email=request.data['email'], password=request.data['password'])

    if user is not None:
        login(request, user)
        return JsonResponse({'email': user.email, 'username': user.username})
    else:
        return Response(status=HTTP_404_NOT_FOUND)

@swagger_auto_schema(method='get', responses={200: UserSmallSerializer})
@swagger_auto_schema(method='patch', request_body=UserSmallSerializer, responses={200: UserSmallSerializer})
@api_view(['GET', 'PATCH'])
def get_auth(request: WSGIRequest) -> Response:
    user = request.user
    print(request.method)
    if request.method == 'GET':
        if (user.is_authenticated):
            return Response({ 'email': user.email, 'username': user.username })
    elif request.method == 'PATCH':
        validateUser = UserSmallSerializer(data=request.data)
        if validateUser.is_valid():
            user.email = request.data['email']
            user.username = request.data['username']
            user.save()
            return Response({ 'email': user.email, 'username': user.username })

    return Response(None)

@swagger_auto_schema(method='post', request_body=UserSerializer)
@api_view(['POST'])
def create_user(request: WSGIRequest) -> Response:
    if request.user.is_authenticated:
        return Response(status=HTTP_405_METHOD_NOT_ALLOWED)
    password = request.data['password']
    confirmPassword = request.data['confirmPassword']
    validateUser = UserSerializer(data=request.data)

    if validateUser.is_valid() or validateUser.password != validateUser.confirmPassword:
        user = User(email=request.data['email'], username=request.data['username'])
        user.set_password(request.data['password'])
        try:
            user.save()
        except Exception:
            print(Exception)
            return Response(status=HTTP_400_BAD_REQUEST)
        return Response()
    else:
        return Response(status=HTTP_400_BAD_REQUEST)

@api_view(['POST'])
def logout_user(request: WSGIRequest) -> Response:
    if not request.user.is_authenticated:
        return Response(status=HTTP_400_BAD_REQUEST)

    logout(request)
    return Response('Successful logout')
