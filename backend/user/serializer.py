from rest_framework import serializers
from .models import User

class UserSerializer(serializers.Serializer):
    username = serializers.CharField(required=True, max_length=100)
    email = serializers.EmailField(required=True)
    password = serializers.CharField(required=True)
    confirmPassword = serializers.CharField(required=True)

class UserLoginSerializer(serializers.Serializer):
    email = serializers.EmailField(required=True)
    password = serializers.CharField(required=True)

class UserSmallSerializer(serializers.Serializer):

    username = serializers.CharField(required=True, max_length=100)
    email = serializers.EmailField(required=True)
