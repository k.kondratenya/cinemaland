from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils import timezone
import os

def get_file_path(instance, filename):
    filename = instance.username
    return os.path.join('uploads/avatarts', filename)

class User(AbstractUser):

    email = models.EmailField(verbose_name='Почта', unique=True)
    avatar = models.ImageField(verbose_name='Аватар', null=True, upload_to=get_file_path)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']
    class Meta:
        db_table = 'User'
        verbose_name = 'User'

    def __str__(self) -> str:
        return self.username
