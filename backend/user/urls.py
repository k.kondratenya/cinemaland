# from django.contrib import admin
from django.urls import path

from .views import login_user, create_user, logout_user, get_auth

urlpatterns = [
    path('', get_auth),
    path('login/', login_user),
    path('sign-up/', create_user),
    path('logout/', logout_user),
]
