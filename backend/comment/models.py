from django.db import models
from user.models import User
from film.models import Film
from django.core.validators import MaxValueValidator, MinValueValidator
from datetime import datetime

class Comment(models.Model):

    film = models.ForeignKey(Film, related_name='comments', on_delete=models.CASCADE)
    user = models.ForeignKey(User, related_name='comments',  on_delete=models.CASCADE)
    content = models.TextField(verbose_name='Content')
    date = models.DateTimeField(verbose_name='Date', default=datetime.now, blank=True)

    class Meta:
        db_table = 'Comment'
        verbose_name = 'Comment'


class CommentRate(models.Model):

    user = models.ForeignKey(User, related_name='comment_rate',  on_delete=models.CASCADE)
    comment = models.ForeignKey(Comment, related_name='rate', on_delete=models.CASCADE)
    rate = models.IntegerField(verbose_name='rate', validators=[MinValueValidator(-1), MaxValueValidator(1)])

    class Meta:
        db_table = 'CommentRate'
        verbose_name = 'CommentRate'
