from django.shortcuts import render
from django.core.handlers.wsgi import WSGIRequest
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.status import (
    HTTP_200_OK, HTTP_201_CREATED, HTTP_204_NO_CONTENT,
    HTTP_400_BAD_REQUEST, HTTP_403_FORBIDDEN, HTTP_404_NOT_FOUND, HTTP_405_METHOD_NOT_ALLOWED,
    HTTP_503_SERVICE_UNAVAILABLE,
)
from django.contrib.auth.decorators import login_required
from django.conf import settings
from django.core.paginator import Paginator
from django.forms.models import model_to_dict
from django.db.models import Prefetch, F, Sum
from django.db.models.functions import Coalesce
from .models import Comment, CommentRate
from film.models import Film
from user.models import User
from .serializer import GetCommentSerializer, PostCommentSerializer, CommentRateSerializer
from django.shortcuts import get_object_or_404
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi

@swagger_auto_schema(method='get', responses={200: GetCommentSerializer(many=True)})
@swagger_auto_schema(method='post', request_body=PostCommentSerializer, responses={200: GetCommentSerializer})
@api_view(['GET', 'POST'])
def comments(request: WSGIRequest, film_id) -> Response:
    if request.method == 'GET':

        comments = Comment.objects.filter(film__pk=film_id).order_by('-date').all()

        paginator = Paginator(comments, 10)
        page = paginator.page(1)
        comments = page.object_list.annotate(username=F('user__username')).annotate(sumRate = Coalesce(Sum('rate__rate'), 0)).all()
        comments_result = list(comments.values('id', 'username', 'date', 'content', 'sumRate'))

        if request.user.is_authenticated:
            comments = comments.prefetch_related(Prefetch('rate', queryset=CommentRate.objects.filter(user=request.user))).all()
            for index, comment in enumerate(comments):
                comment_rate = comment.rate.values('rate')
                comments_result[index]['rate'] = comment_rate[0]['rate'] if not len(comment_rate) == 0 else 0

        return Response(comments_result)

    if request.method == 'POST':
        if not request.user.is_authenticated:
            return Response(status=HTTP_403_FORBIDDEN)

        serializer = PostCommentSerializer(data=request.data)
        if serializer.is_valid():
            data = serializer.data
            film = get_object_or_404(Film, pk=film_id)
            user = request.user
            comment = Comment(film=film, user=user, content=data['content'])
            comment.save()
            comment_res = model_to_dict(comment)
            comment_res['username'] = comment.user.username
            comment_res['sumRate'] = 0
            comment_res['rate'] = 0
            return Response(comment_res)
        else:
            return Response(status=HTTP_400_BAD_REQUEST)


@swagger_auto_schema(method='post', request_body=CommentRateSerializer)
@api_view(['POST'])
@permission_classes([IsAuthenticated])
def set_comment_rate(request: WSGIRequest, id) -> Response:
    user = request.user
    comment = get_object_or_404(Comment, pk=id)

    comment_rate = CommentRate.objects.filter(user=user).filter(comment=comment).first()
    rate = request.data['rate']

    if comment_rate is None:
        comment_rate = CommentRate(user=user, comment=comment, rate=rate)
        comment_rate.save()
    else:
        if rate == 0:
            comment_rate.delete()
        else:
            comment_rate.rate = rate
            comment_rate.save()


    return Response()
