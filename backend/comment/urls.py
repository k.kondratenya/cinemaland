# from django.contrib import admin
from django.urls import path

from .views import comments, set_comment_rate

urlpatterns = [
    # path('', comments),
    path('<int:film_id>/', comments),
    path('<int:id>/rate/', set_comment_rate),
]
