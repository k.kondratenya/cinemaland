from rest_framework import serializers
from .models import Comment

class PostCommentSerializer(serializers.Serializer):
    content = serializers.CharField()

class GetCommentSerializer(serializers.ModelSerializer):
    username = serializers.CharField()
    sumRate = serializers.IntegerField()
    rate = serializers.IntegerField()

    class Meta:
        model = Comment
        fields = ('id', 'content', 'date', 'username', 'sumRate', 'rate')

class CommentRateSerializer(serializers.Serializer):
    rate = serializers.IntegerField()
