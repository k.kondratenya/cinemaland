from django.contrib import admin

from .models import Comment, CommentRate


class CommentAdmin(admin.ModelAdmin):
    list_display = ('user', 'film', 'content')


# Register your models here.
admin.site.register(Comment, CommentAdmin)

class CommentRateAdmin(admin.ModelAdmin):
    list_display = ('user', 'comment', 'rate')


# Register your models here.
admin.site.register(CommentRate, CommentRateAdmin)
