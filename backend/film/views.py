from django.shortcuts import render
from django.core.handlers.wsgi import WSGIRequest
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.response import Response
from rest_framework.status import (
    HTTP_200_OK, HTTP_201_CREATED, HTTP_204_NO_CONTENT,
    HTTP_400_BAD_REQUEST, HTTP_403_FORBIDDEN, HTTP_404_NOT_FOUND, HTTP_405_METHOD_NOT_ALLOWED,
    HTTP_503_SERVICE_UNAVAILABLE,
)
from django.conf import settings
from django.core.paginator import Paginator
from django.core import serializers
from django.forms.models import model_to_dict
from django.db.models import Prefetch, F
import csv
import os
from .models import Film, FilmActions
from .serializer import FilmActionsSerializer, FilmRateSerializer, FilmRateSerializerResponse, FilmResponse, FilmResponseBig, FilmResponseSmall

from django.shortcuts import get_object_or_404
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi
from random import randint

@swagger_auto_schema(method='get', responses={200: FilmResponseSmall(many=True)})
@api_view(['GET'])
def films(request: WSGIRequest) -> Response:
    films = Film.objects.all()
    paginator = Paginator(films, 20)

    page_number = randint(1, paginator.num_pages)
    page = paginator.page(page_number)

    if (not request.user.is_authenticated):
        films_result = page.object_list.values()
    else:
        films = page.object_list.prefetch_related(Prefetch('film_actions', queryset=FilmActions.objects.filter(user=request.user))).all()
        films_result = []
        for film in films:
            film_dict = model_to_dict(film)
            film_actions = film.film_actions.values('isFavorite', 'watchLater')
            default_dict = {
                'isFavorite': False,
                'watchLater': False,
            }
            film_dict.update(film_actions[0] if not len(film_actions) == 0 else default_dict)
            films_result.append(film_dict)

    return Response(films_result)


@swagger_auto_schema(method='get', responses={200: FilmResponseBig})
@api_view(['GET'])
def film(request: WSGIRequest, id) -> Response:
    film = get_object_or_404(Film, pk=id)

    film_persons = film.film_persons.annotate(name = F('person__name')).annotate(personID = F('person__id')).values('personID', 'name', 'job', 'role')

    if (request.user.is_authenticated):
        film_actions = film.film_actions.filter(user=request.user).values('isFavorite', 'watchLater', 'watched', 'userRating')
    else:
        film_actions = []

    default_dict = {
        'isFavorite': False,
        'watchLater': False,
        'watched': False,
        'userRating': None,
    }

    film_dict = model_to_dict(film)

    film_dict.update(film_actions[0] if not len(film_actions) == 0 else default_dict)

    film_dict['persons'] = film_persons

    return Response(film_dict)


search_param = openapi.Parameter('q', openapi.IN_QUERY, description="test manual param", type=openapi.TYPE_STRING)

@swagger_auto_schema(method='get', manual_parameters=[search_param], responses={200: FilmResponse(many=True)})
@api_view(['GET'])
def film_search(request: WSGIRequest) -> Response:
    query = request.GET.get('q', '')
    films = Film.objects.filter(title__contains=query)[:10].values()
    return Response(films)


@swagger_auto_schema(method='post', request_body=FilmActionsSerializer)
@api_view(["POST"])
@permission_classes([IsAuthenticated])
def film_action(request: WSGIRequest, id) -> Response:

    data = request.data
    user = request.user
    film = get_object_or_404(Film, pk=id)

    serializer = FilmActionsSerializer(data=data, partial=True)

    if serializer.is_valid():
        data = serializer.data

        film_action = FilmActions.objects.filter(user=request.user).filter(film__pk=id).first()
        if (film_action is not None):
            for field in data:
                setattr(film_action, field, data[field])
            film_action.save()
            return Response()
        film_action = FilmActions(film=film, user=user, **data)
        film_action.save()
        return Response()
    return Response(status=HTTP_400_BAD_REQUEST)


@swagger_auto_schema(method='post', request_body=FilmRateSerializer, responses={200: FilmRateSerializerResponse})
@api_view(["POST"])
@permission_classes([IsAuthenticated])
def film_rating(request: WSGIRequest, id) -> Response:

    if request.method == 'POST':
        data = request.data
        user = request.user
        film = get_object_or_404(Film, pk=id)

        serializer = FilmRateSerializer(data=data, partial=True)

        if serializer.is_valid():
            data = serializer.data
            print(data)

            film_action = FilmActions.objects.filter(user=request.user).filter(film__pk=id).first()
            if (film_action is not None):
                watched = data.get('watched')
                user_rating = data.get('userRating')

                if watched is not None:
                    if watched:
                        film_action.watched = watched
                        film_action.userRating = None
                    else:
                        film_action.watched = watched
                        film_action.userRating = user_rating if user_rating is not None == 0 else None
                elif user_rating is not None:
                    film_action.watched = True
                    film_action.userRating = user_rating

                film_action.save()
                film_rate = {
                    'watched': film_action.watched,
                    'userRating': film_action.userRating,
                }
                return Response(film_rate)
            film_action = FilmActions(film=film, user=user, **data)
            film_action.save()
            return Response()
        return Response(status=HTTP_400_BAD_REQUEST)

    return Response()

@swagger_auto_schema(method='get', auto_schema=None)
@api_view(['GET'])
@permission_classes([IsAdminUser])
def init(request: WSGIRequest) -> Response:

    the_file = (str(settings.BASE_DIR) + '/film/csv2.csv')
    filename = os.path.basename(os.path.join(str(settings.BASE_DIR), '/film/csv2.csv'))
    with open(the_file) as obj:
        reader = csv.reader(obj, delimiter=",")
        data = list(reader)
        for film in data[1:]:

            title = film[1]
            year = film[2]
            released = film[4]
            runtime = film[5]
            genre = film[6]
            director=film[7]
            plot = film[10]
            poster = film[14]
            rating = float(film[17])
            imdbID = film[19]
            boxOffice = film[22]
            production = film[23]

            film_db = Film(
                title=title,
                year=year,
                released=released,
                runtime=runtime,
                genre=genre,
                director=director,
                plot=plot,
                poster=poster,
                imdbID = imdbID,
                imdbRating=rating,
                boxOffice=boxOffice,
                production=production,
            )
            print(title, year, released, runtime, genre)
            try:
                film_db.save()
            except:
                continue
    return Response()
