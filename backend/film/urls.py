# from django.contrib import admin
from django.urls import path

from .views import init, films, film, film_action, film_rating, film_search

urlpatterns = [
    path('', films),
    path('search', film_search),
    path('<int:id>/', film),
    path('<int:id>/action/', film_action),
    path('<int:id>/rate/', film_rating),
    path('init/', init),
]
