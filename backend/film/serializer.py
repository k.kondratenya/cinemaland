from rest_framework import serializers
from .models import Film
class FilmActionsSerializer(serializers.Serializer):
    isFavorite = serializers.BooleanField(default=False)
    watchLater = serializers.BooleanField(default=False)

class FilmRateSerializer(serializers.Serializer):
    watched = serializers.BooleanField(default=False)
    userRating = serializers.IntegerField(allow_null=True)

class FilmRateSerializerResponse(serializers.Serializer):
    watched = serializers.BooleanField()
    userRating = serializers.IntegerField()

class FilmResponse(serializers.ModelSerializer):

    class Meta:
        model = Film
        fields = "__all__"

class FilmResponseSmall(serializers.ModelSerializer):

    class Meta:
        model = Film
        fields = "__all__"

    watched = serializers.BooleanField()
    userRating = serializers.IntegerField()

class FilmResponseBig(serializers.ModelSerializer):

    class Meta:
        model = Film
        fields = "__all__"

    watched = serializers.BooleanField()
    userRating = serializers.IntegerField()
    isFavorite = serializers.BooleanField()
    watchLater = serializers.BooleanField()
