from django.contrib import admin

from .models import Film, FilmActions


class FilmAdmin(admin.ModelAdmin):
    list_display = ('title', 'year')


# Register your models here.
admin.site.register(Film, FilmAdmin)

class FilmActionsAdmin(admin.ModelAdmin):
    list_display = ('user', 'film', 'isFavorite', 'userRating')


# Register your models here.
admin.site.register(FilmActions, FilmActionsAdmin)
