from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from user.models import User

class Film(models.Model):

    title = models.CharField(verbose_name='Title', max_length=255)
    year = models.IntegerField(verbose_name='Year')
    released = models.CharField(verbose_name='Released', max_length=255)
    runtime = models.CharField(verbose_name='Run time', max_length=255)
    genre = models.CharField(verbose_name='Genre', max_length=255)
    director = models.CharField(verbose_name='Director', max_length=255)
    plot = models.TextField(verbose_name='Plot')
    country = models.CharField(verbose_name='Country', max_length=255)
    poster = models.CharField(verbose_name='Poster', max_length=255)
    imdbID = models.CharField(max_length=255)
    imdbRating = models.FloatField(verbose_name='ImdbRating', validators=[MinValueValidator(0.0), MaxValueValidator(10)],)
    boxOffice = models.CharField(verbose_name='BoxOffice', max_length=255)
    production = models.CharField(verbose_name='Production', max_length=255)

    REQUIRED_FIELDS=['title', 'year', 'released', 'genre', 'director', 'actors', 'plot', 'poster']

    class Meta:
        db_table = 'Film'
        verbose_name = 'Film'

    def __str__(self) -> str:
        return self.title

class FilmActions(models.Model):

    film = models.ForeignKey(Film, related_name='film_actions', on_delete=models.CASCADE)
    user = models.ForeignKey(User, related_name='film_actions',  on_delete=models.CASCADE)
    isFavorite = models.BooleanField(default=False)
    watchLater = models.BooleanField(default=False)
    watched = models.BooleanField(default=False)
    userRating = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(10)], null=True)


    class Meta:
        db_table = 'FilmAction'
        verbose_name = 'Film action'

    def __str__(self) -> str:
        return self.film.title
